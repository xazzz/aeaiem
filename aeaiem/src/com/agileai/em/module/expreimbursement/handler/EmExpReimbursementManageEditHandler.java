	package com.agileai.em.module.expreimbursement.handler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.common.AppConfig;
import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.ProcessHelper;
import com.agileai.em.module.expreimbursement.service.EmExpReimbursementManage;
import com.agileai.em.wsproxy.BizAttribute;
import com.agileai.em.wsproxy.LaunchWorkItem;
import com.agileai.em.wsproxy.ProcessService;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class EmExpReimbursementManageEditHandler
        extends StandardEditHandler {
	public static final String PROCESS_CODE = "Expreimbursement";
    public EmExpReimbursementManageEditHandler() {
        super();
        this.listHandlerClass = EmExpReimbursementManageListHandler.class;
        this.serviceId = buildServiceId(EmExpReimbursementManage.class);
    }
    public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		User user = (User) this.getUser();
		if(OperaType.CREATE.equals(operaType)){
			setAttribute("ER_PERSON", user.getUserId());
			setAttribute("ER_PERSON_NAME", user.getUserName());
			setAttribute("ER_DATE",DateUtil.getDate(DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, new Date())));
			setAttribute("ER_MONEY", "0.00");
			setAttribute("showEdit",true);
			String invokeFrom = param.get("invokeFrom");
			setAttribute("invokeFrom", invokeFrom);
		}
		if (isReqRecordOperaType(operaType)){
			param.put("ER_ID",param.getString("BizId"));
			DataRow record = getService().getRecord(param);
			setAttribute("showFlow",true);
			if("UNSUBMITTED".equals(record.getString("ER_STATE"))){
				setAttribute("showEdit",true);
				setAttribute("showSubmit",true);
				setAttribute("showFlow",false);
			}
			String invokeFrom = param.get("invokeFrom");
			setAttribute("invokeFrom", invokeFrom);
			setAttributes(param);
			this.setAttributes(record);	
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    protected void processPageAttributes(DataParam param) {
        setAttribute("ER_STYLE",FormSelectFactory.create("ER_STYLE")
        		.addSelectedValue(getAttributeValue("ER_STYLE","")));
        setAttribute("ER_STATE",FormSelectFactory.create("EM_STATE")
        		.addSelectedValue(getAttributeValue("ER_STATE","UNSUBMITTED")));
    }
    
    @PageAction
    public ViewRenderer save(DataParam param)throws Exception{
		String operateType = param.get(OperaType.KEY);
		User user = (User) this.getUser();
		if (OperaType.CREATE.equals(operateType)){
			getService().createRecord(param);
			ProcessHelper processHelper = ProcessHelper.instance();
	   		ProcessService processService = processHelper.getBPMService();
	   		String uuId = param.get("ER_ID");
	   		processService.saveDraft(PROCESS_CODE, uuId,param.getString("ER_TITLE"),user.getUserCode());
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updateRecord(param);	
		}
		param.put("operaType", "update");
		return prepareDisplay(param);
	}
    
    @PageAction
   	public ViewRenderer submit(DataParam param) throws Exception {
   		User user = (User)getUser();
   		param.put("ER_STATE", "AUDITING");
   		getService().updateRecord(param);
   		
   		List<BizAttribute> attributeList = new ArrayList<BizAttribute>();
   		BizAttribute bizAttribute = new BizAttribute();
   		bizAttribute.setCode("userId");
   		bizAttribute.setValue(user.getUserId());
   		attributeList.add(bizAttribute);
   		String bizRecordId = param.get("ER_ID");
   		if(StringUtil.isNullOrEmpty(bizRecordId)){
   			bizRecordId = KeyGenerator.instance().genKey();
   		}
   		ProcessHelper processHelper = ProcessHelper.instance();
   		ProcessService processService = processHelper.getBPMService();
   		String processId = processService.getCurrentProcessId(PROCESS_CODE);
   		String title = param.get("ER_TITLE");
   		LaunchWorkItem launchWorkItem = processHelper.createLaunchWorkItem(processId);
   		launchWorkItem.getAttributeList().addAll(attributeList);
   		launchWorkItem.setTitle(title);
   		launchWorkItem.setBizRecordId(bizRecordId);
   		launchWorkItem.setUserCode(user.getUserCode());
   		param.put("SKIP_FIRST_NODE", "true");
   		boolean skipFirstNode = "true".equals(param.get("SKIP_FIRST_NODE"));
   		processService.launchProcess(launchWorkItem,skipFirstNode);
   		return prepareDisplay(param);
   	}
    
    @PageAction
	public ViewRenderer retrieveIds(DataParam param) throws JSONException{
		String responseText = "";
		AppConfig appConfig = BeanFactory.instance().getAppConfig();
   		String BpmShowFlowUrl = appConfig.getConfig("GlobalConfig", "BpmShowFlowUrl");
		ProcessHelper processHelper = ProcessHelper.instance();
		ProcessService bpmService = processHelper.getBPMService();
		String bizRecordId = param.getString("ER_ID");
		List<String> aa = bpmService.getProcessId8InstId(PROCESS_CODE, bizRecordId);
		JSONObject jsonObject = new JSONObject();  
        jsonObject.put("processId", aa.get(0));
        jsonObject.put("processInstId", aa.get(1));
        jsonObject.put("BpmShowFlowUrl", BpmShowFlowUrl);
        String json = jsonObject.toString();
		responseText=json;
		return new AjaxRenderer(responseText);
	}
    
    protected EmExpReimbursementManage getService() {
        return (EmExpReimbursementManage) this.lookupService(this.getServiceId());
    }
}
