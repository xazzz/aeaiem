package com.agileai.em.module.expreimbursement.handler;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.ProcessHelper;
import com.agileai.em.module.expreimbursement.service.EmExpReimbursementManage;
import com.agileai.em.wsproxy.BizAttribute;
import com.agileai.em.wsproxy.ProcessService;
import com.agileai.em.wsproxy.SubmitWorkItem;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;

public class EmExpReimbursementCashierManageHandler
        extends StandardEditHandler {
    public EmExpReimbursementCashierManageHandler() {
        super();
        this.listHandlerClass = EmExpReimbursementManageListHandler.class;
        this.serviceId = buildServiceId(EmExpReimbursementManage.class);
    }
    public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		param.put("ER_ID",param.get("WFE_ID"));
		DataRow record = getService().getRecord(param);
		this.setAttributes(record);	
		List<DataRow> records = getService().findApproveOpinionRecords(param);
		setAttribute("ApproceOpinionRecords", records);
		this.setOperaType(operaType);
		setAttributes(param);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    protected void processPageAttributes(DataParam param) {
        setAttribute("ER_STYLE",FormSelectFactory.create("ER_STYLE")
        		.addSelectedValue(getAttributeValue("ER_STYLE","")));
        setAttribute("ER_STATE",FormSelectFactory.create("EM_STATE")
        		.addSelectedValue(getAttributeValue("ER_STATE","UNSUBMITTED")));
        setAttribute("EXP_APP_RESULT", FormSelectFactory.create("APPOPERTYPE")
				.addSelectedValue(getAttributeValue("EXP_APP_RESULT", "Y")));
        
    }
    @PageAction
    public ViewRenderer submit(DataParam param) {
		String responseText = FAIL;
		try {
			User user = (User) getUser();
			DataParam newParam = new DataParam();
			newParam.put("ER_ID",param.getString("ER_ID"));
			newParam.put("ER_STATE", "PAID");
	   		getService().changeStateRecord(newParam);
			
			List<BizAttribute> attributeList = new ArrayList<BizAttribute>();
			BizAttribute bizAttribute = new BizAttribute();
	   		bizAttribute.setCode("userId");
	   		bizAttribute.setValue(user.getUserId());
	   		attributeList.add(bizAttribute);
	   		
	   		ProcessHelper processHelper = ProcessHelper.instance();
			ProcessService bpmService = processHelper.getBPMService();
			String processId = param.getString("WFP_ID");
			String processInstId = param.getString("WFIP_ID");
			String activityCode = param.getString("WFA_CODE");
			SubmitWorkItem processWorkItem = processHelper.createSubmitWorkItem(processId, processInstId, activityCode);
			if(StringUtil.isNullOrEmpty(attributeList.get(0).getCode())){
				attributeList = processWorkItem.getAttributeList();
			}
			processWorkItem.setUserCode(user.getUserCode());
			processWorkItem.getAttributeList().addAll(attributeList);
			bpmService.submitProcess(processWorkItem);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		responseText = SUCCESS;
		return new AjaxRenderer(responseText);
	}
    protected EmExpReimbursementManage getService() {
        return (EmExpReimbursementManage) this.lookupService(this.getServiceId());
    }
}
