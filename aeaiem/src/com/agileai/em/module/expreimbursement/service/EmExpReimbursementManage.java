package com.agileai.em.module.expreimbursement.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface EmExpReimbursementManage
        extends StandardService {

	List<DataRow> findApproveOpinionRecords(DataParam param);

	void createApproveRecord(DataParam queryParam);

	void changeStateRecord(DataParam newParam);
}
