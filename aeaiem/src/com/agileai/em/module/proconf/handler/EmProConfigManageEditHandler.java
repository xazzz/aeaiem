package com.agileai.em.module.proconf.handler;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.PrivilegeHelper;
import com.agileai.em.module.proconf.service.EmProConfigManage;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class EmProConfigManageEditHandler
        extends StandardEditHandler {
    public EmProConfigManageEditHandler() {
        super();
        this.listHandlerClass = EmProConfigManageListHandler.class;
        this.serviceId = buildServiceId(EmProConfigManage.class);
    }

    public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		User user = (User) this.getUser();
		PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
		if(OperaType.CREATE.equals(operaType)){
			setAttribute("PC_MONEY", "0.00");
			setAttributes(param);
			setAttribute("showEdit",true);
			setAttribute("canEdit",true);
		}
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getRecord(param);
			setAttribute("PC_STYLE_READ",record.getString("PC_STYLE"));
			this.setAttributes(record);
			if("INIT".equals(record.getString("PC_STATE"))){
				if(privilegeHelper.isManager()){
					setAttribute("showEdit",true);
					setAttribute("canEdit",true);
					setAttribute("showSubmit",true);
				}
				if(privilegeHelper.isProleader()){
					setAttribute("showEdit",true);
					setAttribute("canEdit",true);
					setAttribute("showSubmit",true);
				}
			}else{
				if(privilegeHelper.isManager()){
					setAttribute("showEdit",true);
				}
			}
			if("SUBMIT".equals(record.getString("PC_STATE"))){
				if(privilegeHelper.isManager()){
					setAttribute("showResubmit",true);
				}
			}
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    @PageAction
   	public ViewRenderer showPcCode(DataParam param) {
    	//类型编码
    	String pcStyle = param.getString("PC_STYLE");
    	//年月日
    	String year =DateUtil.getYear();
    	String month = DateUtil.getMonth();
    	String day = DateUtil.getDay();
    	
    	String YYYY = year+month+day;
    	
    	DataRow record =getService().showNumRecord(param);
    	String strNum = record.getString("strNum");
    	String pcCode = pcStyle+"-"+YYYY+"-"+strNum;
    	param.put("PC_CODE", pcCode);
    	return prepareDisplay(param);
    }
    protected void processPageAttributes(DataParam param) {
    	setAttribute("PC_STYLE",
                FormSelectFactory.create("PC_STYLE")
                                 .addSelectedValue(getAttributeValue("PC_STYLE",
                                                                          "")));
    	setAttribute("PC_STATE",
                FormSelectFactory.create("PC_STATE")
                                 .addSelectedValue(getAttributeValue("PC_STATE",
                                                                          "INIT")));
    }
    
    public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			param.put("PC_STYLE",param.get("PC_STYLE_READ"));
			getService().updateRecord(param);	
		}
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
    
    @PageAction
	public ViewRenderer submit(DataParam param){
    	param.put("PC_STATE","SUBMIT");
    	param.put("PC_STYLE",param.get("PC_STYLE_READ"));
    	getService().updateRecord(param);	
    	return new RedirectRenderer(getHandlerURL(listHandlerClass));
    }
    @PageAction
	public ViewRenderer resubmit(DataParam param){
    	param.put("PC_STATE","INIT");
    	param.put("PC_STYLE",param.get("PC_STYLE_READ"));
    	getService().updateRecord(param);	
    	return new RedirectRenderer(getHandlerURL(listHandlerClass));
    }
    protected EmProConfigManage getService() {
        return (EmProConfigManage) this.lookupService(this.getServiceId());
    }
}
