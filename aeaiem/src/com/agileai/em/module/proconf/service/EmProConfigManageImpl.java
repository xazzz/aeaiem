package com.agileai.em.module.proconf.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class EmProConfigManageImpl  extends StandardServiceImpl  implements EmProConfigManage {
    public EmProConfigManageImpl() {
        super();
    }
    public List<DataRow> findRecords(DataParam param) {
    	param.put("pcCodeEx","%"+param.get("pcCode")+"%");
    	param.put("pcNameEx","%"+param.get("pcName")+"%");
		String statementId = sqlNameSpace+"."+"findRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
    public void createRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertRecord";
		processDataType(param, tableName);
		processPrimaryKeys(param);
		this.daoHelper.insertRecord(statementId, param);
	}
    public DataRow showNumRecord(DataParam param) {
    	//流水号
    	String statementId = sqlNameSpace+"."+"getPcNumRecord";
		DataRow numRow = this.daoHelper.getRecord(statementId, param);
		String num = numRow.get("PC_NUM").toString();
		String strNum = ("00"+num).substring(1);
		if(("00"+num).length()>=4){
			strNum=("00"+num).substring(("00"+num).length()- 2,("00"+num).length());//截取两个数字之间的部分
		}
		DataRow result = new DataRow();
		result.put("strNum",strNum);
		return result;
    }
    
}
