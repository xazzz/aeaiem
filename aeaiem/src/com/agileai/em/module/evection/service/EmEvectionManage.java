package com.agileai.em.module.evection.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;

public interface EmEvectionManage
        extends MasterSubService {

	void computeTotalMoney(String masterRecordId);

	List<DataRow> findApproveOpinionRecords(DataParam param);

	void createApproveRecord(DataParam queryParam);

	void changeStateRecord(DataParam newParam);

	List<DataRow> findPcNameRecords(DataParam param);

}
