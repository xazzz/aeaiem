package com.agileai.em.module.evection.handler;

import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.common.AppConfig;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.PrivilegeHelper;
import com.agileai.em.common.ProcessHelper;
import com.agileai.em.module.evection.service.EmEvectionManage;
import com.agileai.em.wsproxy.ProcessService;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.controller.core.MasterSubListHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class EmEvectionManageListHandler
        extends MasterSubListHandler {
	public static final String PROCESS_CODE = "Emevection";
    public EmEvectionManageListHandler() {
        super();
        this.editHandlerClazz = EmEvectionManageEditHandler.class;
        this.serviceId = buildServiceId(EmEvectionManage.class);
    }
    public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		setAttributes(param);
		User user = (User) this.getUser();
		PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
		if(privilegeHelper.isFinance()||privilegeHelper.isManager()){
			param.put("CANSHOW", "UNSUBMITTED");
			param.put("SHOWPERSON", user.getUserId());
		}else{
			param.put("EVE_APPLICATION", user.getUserId());
		}
		List<DataRow> rsList = getService().findMasterRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {
    	List<DataRow> records = getService().findPcNameRecords(param);
		FormSelect formSelect = new FormSelect();
		formSelect.setKeyColumnName("PC_CODE");
		formSelect.setValueColumnName("PC_NAME");
		formSelect.putValues(records);
		setAttribute("pcName",formSelect.addSelectedValue(param.get("pcName")));
		initMappingItem("PC_NAME",formSelect.getContent());
        setAttribute("eveState",FormSelectFactory.create("EM_STATE")
        		.addSelectedValue(param.get("eveState")));
        initMappingItem("EVE_STATE",
        		FormSelectFactory.create("EM_STATE").getContent());
        initMappingItem("EVP_TRANSPORTATION_WAY",
                FormSelectFactory.create("TRANSPORTATION_WAY").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "eveState", "");
        initParamItem(param,"sdate",DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getBeginOfMonth(new Date()),DateUtil.MONTH,-1)));
 		initParamItem(param,"edate",DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, 1)));
    }

    @PageAction
  	public ViewRenderer retrieveIds(DataParam param) throws JSONException{
  		String responseText = "";
  		AppConfig appConfig = BeanFactory.instance().getAppConfig();
 		String BpmShowFlowUrl = appConfig.getConfig("GlobalConfig", "BpmShowFlowUrl");
 		setAttribute("BpmShowFlowUrl", BpmShowFlowUrl);
  		ProcessHelper processHelper = ProcessHelper.instance();
  		ProcessService bpmService = processHelper.getBPMService();
  		String bizRecordId = param.getString("EVE_ID");
  		List<String> aa = bpmService.getProcessId8InstId(PROCESS_CODE, bizRecordId);
  		JSONObject jsonObject = new JSONObject();  
          jsonObject.put("processId", aa.get(0));
          jsonObject.put("processInstId", aa.get(1));
          jsonObject.put("BpmShowFlowUrl", BpmShowFlowUrl);
          String json = jsonObject.toString();
  		responseText=json;
  		return new AjaxRenderer(responseText);
  	}
    protected EmEvectionManage getService() {
        return (EmEvectionManage) this.lookupService(this.getServiceId());
    }
}
