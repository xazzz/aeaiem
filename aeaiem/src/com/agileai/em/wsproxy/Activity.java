
package com.agileai.em.wsproxy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for activity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="activity">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.agileai.com/bpm/api}defineObject">
 *       &lt;sequence>
 *         &lt;element name="actType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="businessURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="keepLastPerformer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="process" type="{http://www.agileai.com/bpm/api}process" minOccurs="0"/>
 *         &lt;element name="processId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refProcessId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="scriptlet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transInType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transOutType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "activity", propOrder = {
    "actType",
    "businessURL",
    "keepLastPerformer",
    "process",
    "processId",
    "refProcessId",
    "scriptlet",
    "transInType",
    "transOutType"
})
public class Activity
    extends DefineObject
{

    protected String actType;
    protected String businessURL;
    protected boolean keepLastPerformer;
    protected Process process;
    protected String processId;
    protected String refProcessId;
    protected String scriptlet;
    protected String transInType;
    protected String transOutType;

    /**
     * Gets the value of the actType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActType() {
        return actType;
    }

    /**
     * Sets the value of the actType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActType(String value) {
        this.actType = value;
    }

    /**
     * Gets the value of the businessURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessURL() {
        return businessURL;
    }

    /**
     * Sets the value of the businessURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessURL(String value) {
        this.businessURL = value;
    }

    /**
     * Gets the value of the keepLastPerformer property.
     * 
     */
    public boolean isKeepLastPerformer() {
        return keepLastPerformer;
    }

    /**
     * Sets the value of the keepLastPerformer property.
     * 
     */
    public void setKeepLastPerformer(boolean value) {
        this.keepLastPerformer = value;
    }

    /**
     * Gets the value of the process property.
     * 
     * @return
     *     possible object is
     *     {@link Process }
     *     
     */
    public Process getProcess() {
        return process;
    }

    /**
     * Sets the value of the process property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process }
     *     
     */
    public void setProcess(Process value) {
        this.process = value;
    }

    /**
     * Gets the value of the processId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessId() {
        return processId;
    }

    /**
     * Sets the value of the processId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessId(String value) {
        this.processId = value;
    }

    /**
     * Gets the value of the refProcessId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefProcessId() {
        return refProcessId;
    }

    /**
     * Sets the value of the refProcessId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefProcessId(String value) {
        this.refProcessId = value;
    }

    /**
     * Gets the value of the scriptlet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScriptlet() {
        return scriptlet;
    }

    /**
     * Sets the value of the scriptlet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScriptlet(String value) {
        this.scriptlet = value;
    }

    /**
     * Gets the value of the transInType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransInType() {
        return transInType;
    }

    /**
     * Sets the value of the transInType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransInType(String value) {
        this.transInType = value;
    }

    /**
     * Gets the value of the transOutType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransOutType() {
        return transOutType;
    }

    /**
     * Sets the value of the transOutType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransOutType(String value) {
        this.transOutType = value;
    }

}
