package com.agileai.em.common;

import java.net.URL;

import com.agileai.common.AppConfig;
import com.agileai.em.wsproxy.LaunchWorkItem;
import com.agileai.em.wsproxy.ProcessService;
import com.agileai.em.wsproxy.ProcessService_Service;
import com.agileai.em.wsproxy.SubmitWorkItem;
import com.agileai.hotweb.common.BeanFactory;

public class ProcessHelper {
	private ProcessService bpmService = null;
	
	private static ProcessHelper instance = new ProcessHelper();
	
	private ProcessHelper(){
		BeanFactory beanFactory = BeanFactory.instance();
		AppConfig appConfig = beanFactory.getAppConfig();
		String serviceURL = appConfig.getConfig("GlobalConfig", "BpmServiceUrl");
		try {
			URL url = new URL(serviceURL);
			ProcessService_Service processService_Service = new ProcessService_Service(url); 
			this.bpmService = processService_Service.getProcessServicePort();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static ProcessHelper instance(){
		return instance;
	}
	
	public ProcessService getBPMService(){
		return this.bpmService;
	}
	
	public LaunchWorkItem createLaunchWorkItem(String processURI){
		LaunchWorkItem bpmLaunchWorkItem = new LaunchWorkItem();
		bpmLaunchWorkItem.setProcessId(processURI);
		return bpmLaunchWorkItem;
	}
	
	public SubmitWorkItem createSubmitWorkItem(String processURI,String procInstanceId,String activityCode){
		SubmitWorkItem processWorkItem = new SubmitWorkItem();
		processWorkItem.setProcessId(processURI);
		processWorkItem.setProcessInstanceId(procInstanceId);
		processWorkItem.setActivityCode(activityCode);
		return processWorkItem;
	}
}