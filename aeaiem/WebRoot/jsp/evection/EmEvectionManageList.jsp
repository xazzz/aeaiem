<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>出差报销</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function controlBtn(stateResult){
	if(stateResult =='UNSUBMITTED'){
		enableButton("editImgBtn");
		enableButton("delImgBtn");
		disableButton("flowImgBtn");
	}
	if(stateResult =='AUDITING'){
		disableButton("editImgBtn");
		disableButton("delImgBtn");
		enableButton("flowImgBtn");
	}
	if(stateResult =='PAID'){
		disableButton("editImgBtn");
		disableButton("delImgBtn");
		enableButton("flowImgBtn");
	}
}
var instanceGraphBox;
function viewInstanceGraphBox(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	postRequest('form1',{actionType:'retrieveIds',onComplete:function(responseText){
		var json = $.parseJSON(responseText);	
		var WFP_ID = json.processId;			
		var WFIP_ID = json.processInstId;
		var BpmShowFlowUrl = json.BpmShowFlowUrl;
	if (!instanceGraphBox){
			instanceGraphBox = new PopupBox('instanceGraphBox','业务流程图',{size:'normal',width:'900px',height:'500px',top:'3px',scroll:'yes'});
		}
		var url = BpmShowFlowUrl+"&actionType=prepareDisplay&WFIP_ID="+WFIP_ID+"&WFP_ID="+WFP_ID;
		instanceGraphBox.sendRequest(url);			
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolBar" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" id="editImgBtn"/>编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" id="detailImgBtn"/>查看</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="viewInstanceGraphBox('')"><input value="&nbsp;" type="button" class="flowImgBtn" id="flowImgBtn" title="流程图" />流程图</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" id="delImgBtn"/>删除</td>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;状态<select id="eveState" label="状态" name="eveState" class="select" onchange="doQuery()"><%=pageBean.selectValue("eveState")%></select>
&nbsp;项目名称<select id="pcName" label="项目名称" name="pcName" class="select" onchange="doQuery()"><%=pageBean.selectValue("pcName")%></select>

&nbsp;开始日期<input id="sdate" label="开始时间" name="sdate" type="text" value="<%=pageBean.inputDate("sdate")%>" size="10" class="text" readonly="readonly"/><img id="sdatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
-<input id="edate" label="截止时间" name="edate" type="text" value="<%=pageBean.inputDate("edate")%>" size="10" class="text"  readonly="readonly"/><img id="edatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />

&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="出差报销.csv"
retrieveRowsCallback="process" xlsFileName="出差报销.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,{EVE_ID:'${row.EVE_ID}'});refreshConextmenu();controlBtn('${row.EVE_STATE}');" onclick="selectRow(this,{EVE_ID:'${row.EVE_ID}'});controlBtn('${row.EVE_STATE}');">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="PC_NAME" title="项目名称"   />
	<ec:column width="100" property="EVE_APPLICATION_NAME" title="出差人"   />
	<ec:column width="100" property="EVE_TOGETHER" title="同行人"   />
	<ec:column width="100" property="EVE_START_TIME" title="开始日期" cell="date" format="yyyy-MM-dd" />
	<ec:column width="100" property="EVE_OVER_TIME" title="结束日期" cell="date" format="yyyy-MM-dd" />
	<ec:column width="100" property="EVE_REIMBURSEMENT_TIME" title="报销日期" cell="date" format="yyyy-MM-dd" />
	<ec:column width="100" property="EVE_DAYS" title="出差天数"   />
	<ec:column width="100" property="EVE_SUBSIDY" title="补助金额"   />
	<ec:column width="100" property="EVE_TOTAL_MONEY" title="汇总费用"   />
	<ec:column width="100" property="EVE_STATE" title="状态"   mappingItem="EVE_STATE"/>
</ec:row>
</ec:table>
<input type="hidden" name="EVE_ID" id="EVE_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('EVE_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
initCalendar('sdate','%Y-%m-%d','sdatePicker');
datetimeValidators[0].set("yyyy-MM-dd").add("sdate");
initCalendar('edate','%Y-%m-%d','edatePicker');
datetimeValidators[0].set("yyyy-MM-dd").add("edate");
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
