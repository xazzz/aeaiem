<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>费用报销</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<style type="text/css">
.markable{
	background-color:yellow;
	color:black;
}
</style>
<script language="javascript">
function changeStateRecord(actionType){
	doSubmit({actionType:actionType});
}
var instanceGraphBox;
function viewInstanceGraphBox(){
	postRequest('form1',{actionType:'retrieveIds',onComplete:function(responseText){
			var json = $.parseJSON(responseText);	
			var WFP_ID = json.processId;			
			var WFIP_ID = json.processInstId;
			var BpmShowFlowUrl = json.BpmShowFlowUrl;
		if (!instanceGraphBox){
				instanceGraphBox = new PopupBox('instanceGraphBox','业务流程图',{size:'normal',width:'900px',height:'500px',top:'3px',scroll:'yes'});
			}
			var url = BpmShowFlowUrl+"&actionType=prepareDisplay&WFIP_ID="+WFIP_ID+"&WFP_ID="+WFP_ID;
			instanceGraphBox.sendRequest(url);			
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("showEdit")){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
<%}%>
<%if(pageBean.getBoolValue("showSubmit")){ %>  
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="changeStateRecord('submit')"><input value="&nbsp;" type="button" class="submitImgBtn" id="submitImgBtn" title="提交" />提交</td>
<%}%>  
<%if(pageBean.getBoolValue("showFlow")){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="viewInstanceGraphBox('')"><input value="&nbsp;" type="button" class="flowImgBtn" id="flowImgBtn" title="流程图" />流程图</td>
<% }%>
<%if(!"BPM".equals(pageBean.getStringValue("invokeFrom"))){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
<% }%>  
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>标题</th>
	<td><input id="ER_TITLE" label="标题" name="ER_TITLE" type="text" value="<%=pageBean.inputValue("ER_TITLE")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>报销人</th>
	<td>
	<input id="ER_PERSON_NAME" label="报销人" name="ER_PERSON_NAME" type="text" value="<%=pageBean.inputValue("ER_PERSON_NAME")%>" readonly="readonly" size="41" class="text" />
	<input id="ER_PERSON" label="报销人" name="ER_PERSON" type="hidden" value="<%=pageBean.inputValue("ER_PERSON")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>报销日期</th>
	<td><input id="ER_DATE" label="报销日期" name="ER_DATE" type="text" value="<%=pageBean.inputDate("ER_DATE")%>" readonly="readonly" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>报销类型</th>
	<td><select id="ER_STYLE" label="报销类型" name="ER_STYLE" class="select"><%=pageBean.selectValue("ER_STYLE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>报销金额</th>
	<td><input id="ER_MONEY" label="报销金额" name="ER_MONEY" class="text markable" type="text" value="<%=pageBean.inputValue("ER_MONEY")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td>
	<input id="ER_STATE_TEXT" label="状态" name="ER_STATE_TEXT"  type="text" value="<%=pageBean.selectedText("ER_STATE")%>" size="41" class="text"/>
	<input id="ER_STATE" label="状态" name="ER_STATE"  type="hidden" value="<%=pageBean.selectedValue("ER_STATE")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>说明</th>
	<td><textarea id="ER_ILLUSTRATE" label="说明" name="ER_ILLUSTRATE" cols="60" rows="5" class="textarea"><%=pageBean.inputValue("ER_ILLUSTRATE")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>备注</th>
	<td><textarea id="ER_REMARKS" label="备注" name="ER_REMARKS" cols="60" rows="5" class="textarea"><%=pageBean.inputValue("ER_REMARKS")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" name="invokeFrom" id="invokeFrom" value="<%=pageBean.inputValue("invokeFrom")%>"/>
<input type="hidden" id="ER_ID" name="ER_ID" value="<%=pageBean.inputValue("ER_ID")%>" />
<input type="hidden" id="PC_ID" name="PC_ID" value="<%=pageBean.inputValue("PC_ID")%>" />
<input type="hidden" id="BpmShowFlowUrl" name="BpmShowFlowUrl" value="<%=pageBean.getAttribute("BpmShowFlowUrl")%>" />
</form>
<script language="javascript">
requiredValidator.add("ER_TITLE");
requiredValidator.add("ER_PERSON");
requiredValidator.add("ER_DATE");
datetimeValidators[0].set("yyyy-MM-dd").add("ER_DATE");
requiredValidator.add("ER_MONEY");
numValidator.add("ER_MONEY");
requiredValidator.add("ER_STYLE");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
