<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>项目配置</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function changeBtn(state){
	if(state=='INIT'){
		enableButton("editImgBtn");
		enableButton("delImgBtn");
	}
	if(state=='SUBMIT'){
		disableButton("editImgBtn");
		disableButton("delImgBtn");
	}
	if(state=='AUDIT'){
		disableButton("editImgBtn");
		disableButton("delImgBtn");
	}
	
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("showEdit")){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" id="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" id="editImgBtn" />编辑</td>
<%} %>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" id="detailImgBtn" />查看</td>   
<%if(pageBean.getBoolValue("showDel")){ %>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" id="delImgBtn" />删除</td>
<%} %>   
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;项目编号<input id="pcCode" label="项目编号" name="pcCode" type="text" value="<%=pageBean.inputValue("pcCode")%>" size="24" class="text" />

&nbsp;项目名称<input id="pcName" label="项目名称" name="pcName" type="text" value="<%=pageBean.inputValue("pcName")%>" size="24" class="text" />

&nbsp;开始日期<input id="sdate" label="开始时间" name="sdate" type="text" value="<%=pageBean.inputDate("sdate")%>" size="10" class="text" readonly="readonly"/><img id="sdatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
-<input id="edate" label="截止时间" name="edate" type="text" value="<%=pageBean.inputDate("edate")%>" size="10" class="text"  readonly="readonly"/><img id="edatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="项目配置.csv"
retrieveRowsCallback="process" xlsFileName="项目配置.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,{PC_ID:'${row.PC_ID}'});refreshConextmenu();changeBtn('${row.PC_STATE}')" onclick="selectRow(this,{PC_ID:'${row.PC_ID}'});changeBtn('${row.PC_STATE}')">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="PC_CODE" title="项目编号"   />
	<ec:column width="100" property="PC_NAME" title="项目名称"   />
	<ec:column width="100" property="PC_STYLE" title="项目类型"   mappingItem="PC_STYLE"/>
	<ec:column width="100" property="PC_FIRST_PARTY" title="甲方"   />
	<ec:column width="100" property="PC_FIRST_PARTY_HEAD" title="甲方负责人"   />
	<ec:column width="100" property="PC_RESPONSIBLE_PERSON" title="实施负责人"   />
	<ec:column width="100" property="PC_SALES_PERSON" title="销售负责人"   />
	<ec:column width="100" property="PC_MONEY" title="项目金额"   />
	<ec:column width="100" property="PC_START_DATE" title="开始日期" cell="date" format="yyyy-MM-dd" />
	<ec:column width="100" property="PC_END_DATE" title="结束日期" cell="date" format="yyyy-MM-dd" />
	<ec:column width="100" property="PC_STATE" title="项目状态"   mappingItem="PC_STATE"/>
</ec:row>
</ec:table>
<input type="hidden" name="PC_ID" id="PC_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('PC_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
initCalendar('sdate','%Y-%m-%d','sdatePicker');
datetimeValidators[0].set("yyyy-MM-dd").add("sdate");
initCalendar('edate','%Y-%m-%d','edatePicker');
datetimeValidators[0].set("yyyy-MM-dd").add("edate");
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
